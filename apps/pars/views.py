import datetime
import json
from django.http import HttpResponse
import urllib.request
from bs4 import BeautifulSoup
from oauth2client.service_account import ServiceAccountCredentials
from apps.pars.models import Customization, Coin, Log


def get_html(url):
    response = urllib.request.urlopen(url)
    return response.read()


def parse(html):
    list_elem = []
    soup = BeautifulSoup(html, features='html.parser')
    table = soup.find('div', class_='row')

    for el in table.find_all('div', class_='db-wrapper'):
        table2 = el.find('div', class_='detail-name')
        table3 = el.find('ul')

        table_li = table3.find_all('li')[0]
        url_img = table_li.find('div', class_='ex-thumbnail-zoom-img animated flipInX')

        material = table3.find_all('li')[1]

        price = table3.find_all('li')[2]

        new_coin = Coin(name=table2.a.text,
                        href=str('https://bank.gov.ua') + str(table2.a.get('href')),
                        url_href=str('https://bank.gov.ua') + str(url_img.get('data-image')),
                        material=material.text,
                        price=price.text
                        )
        list_elem.append(new_coin)

    return list_elem


def get_element(request):
    model = Customization.objects.all().order_by('-id')[0]
    elem = parse(get_html(model.site))
    return HttpResponse(json.dumps(elem, ensure_ascii=False), content_type="application/json")


def _get_access_token():
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        # 'service-account.json', ['https://www.googleapis.com/auth/firebase.messaging'])
        '/home/nout/Script/Python/moneta/service-account.json', ['https://www.googleapis.com/auth/firebase.messaging'])
    access_token_info = credentials.get_access_token()
    return access_token_info.access_token


def post(title, body, image, site_url, number):
    import requests
    import json
    url = "https://fcm.googleapis.com/v1/projects/websellingapp/messages:send"
    data = {
        "message": {
            "topic": "general",
            # "notification": {
            #   "title": "Breaking News",
            #   "body": "New news story available."
            # },
            "data": {
                "title": title,
                "body": body,
                "image": image,
                "url": site_url,
                "number": number,
                "click_action": "MAIN_ACTIVITY"
            },
            # "android": {
            #   "notification": {
            #     "click_action": "TOP_STORY_ACTIVITY",
            #     "body": "Check out the Top Story"
            #   }
            # },
            # "apns": {
            #   "payload": {
            #     "aps": {
            #       "category" : "NEW_MESSAGE_CATEGORY"
            #     }
            #   }
            # }
        }
    }

    headers = {
        'Authorization': 'Bearer ' + _get_access_token(),
        'Content-Type': 'application/json; UTF-8',
    }
    r = requests.post(url, data=json.dumps(data), headers=headers)


def create_elements(elems):
    all_coin_db = Coin.objects.filter(active=True)

    list_el = []
    for all_el in elems:
        list_el.append(all_el.name)

    for coin in all_coin_db:
        if coin.name in list_el:
            print("yes")
        else:
            print('no')
            Coin.objects.filter(pk=coin.pk).update(active=False)

    for el in elems:
        coin = Coin.objects.filter(name=el.name,
                                   href=el.href,
                                   url_href=el.url_href,
                                   material=el.material,
                                   price=el.price,
                                   active=True
                                   )
        if coin.count() < 1:
            print("создать елемент")
            new_coin = Coin(name=el.name,
                            href=el.href,
                            url_href=el.url_href,
                            material=el.material,
                            price=el.price,
                            active=True
                            )
            new_coin.save()

            post("Нові зміни на сайті", "Продукт:" + str(new_coin.name), new_coin.url_href, new_coin.href, str(new_coin.pk))


            new_log = Log(name='Create: ' + str(new_coin.name) + " (" + str(new_coin.pk) + ") ", time=datetime.datetime.now())
            new_log.save()
