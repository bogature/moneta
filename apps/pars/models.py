
from django.db import models
from pytz import unicode


class Customization(models.Model):
    site = models.CharField(max_length=200)

    def __str__(self):
        return unicode(self.site)

    class Meta:
        verbose_name = '1. Настройка'
        verbose_name_plural = '1. Настройка'


class Coin(models.Model):

    name = models.CharField(max_length=400, null=True, blank=True)

    href = models.CharField(max_length=400, null=True, blank=True)

    url_href = models.CharField(max_length=400, null=True, blank=True)

    material = models.CharField(max_length=400, null=True, blank=True)

    price = models.CharField(max_length=400, null=True, blank=True)

    active = models.BooleanField(default=True)

    def __str__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = '1. Монета'
        verbose_name_plural = '1. Монета'


class Log(models.Model):

    name = models.CharField(max_length=200, null=True, blank=True)

    time = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return unicode(self.name)