
from django.contrib import admin
from apps.pars.models import *


class CustomizationAdmin(admin.ModelAdmin):
    list_display = ('id', 'site',)


class CoinAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'href', 'url_href', 'material', 'price', 'active',)


class LogAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'time',)


admin.site.register(Customization, CustomizationAdmin)
admin.site.register(Coin, CoinAdmin)
admin.site.register(Log, LogAdmin)
