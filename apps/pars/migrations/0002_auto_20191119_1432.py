# Generated by Django 2.2.7 on 2019-11-19 14:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pars', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customization',
            name='site',
            field=models.CharField(max_length=200),
        ),
    ]
