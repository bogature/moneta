import datetime
import threading

from django.shortcuts import render

from apps.pars.models import Coin, Log, Customization
from apps.pars.views import post


def home(request):

    site = Customization.objects.all().order_by('-id')[0]
    all_coin_db = Coin.objects.filter(active=True)
    logs = Log.objects.all().order_by('-id')[0:10]

    if request.method == "POST":
        print('start')
        post('Start', 'ssss', 'https://bank.gov.ua/WebSelling/Home/GetImage?idcoin=482&typeimage=Avers', 'https://bank.gov.ua/WebSelling/Home/ViewCoin?idcoin=482')

    return render(request, 'home.html', {'site': site, 'all_coin_db': all_coin_db, 'logs': logs})
