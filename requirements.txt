Django==2.2.7
beautifulsoup4==4.8.1
django-push-notifications==1.6.1
fcm-django==0.3.2
django-crontab==0.7.1
oauth2client