
import datetime
from apps.pars.models import Customization
from apps.pars.views import parse, get_html, create_elements


def my_scheduled_job():
    model = Customization.objects.all().order_by('-id')[0]
    pars_elements = parse(get_html(model.site))
    create_elements(pars_elements)